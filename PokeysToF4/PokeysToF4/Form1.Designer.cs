﻿namespace PokeysToF4
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectListPokeys = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonRead = new System.Windows.Forms.Button();
            this.tableLayoutPokey = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.progressBarBmsRunnning = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonTest = new System.Windows.Forms.Button();
            this.buttonLoadBMSKeystrokes = new System.Windows.Forms.Button();
            this.tableLayoutPokey.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // selectListPokeys
            // 
            this.selectListPokeys.FormattingEnabled = true;
            this.selectListPokeys.Location = new System.Drawing.Point(110, 28);
            this.selectListPokeys.Name = "selectListPokeys";
            this.selectListPokeys.Size = new System.Drawing.Size(222, 21);
            this.selectListPokeys.TabIndex = 0;
            this.selectListPokeys.SelectedIndexChanged += new System.EventHandler(this.comboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pokeys";
            // 
            // buttonRead
            // 
            this.buttonRead.Location = new System.Drawing.Point(346, 27);
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.Size = new System.Drawing.Size(114, 23);
            this.buttonRead.TabIndex = 2;
            this.buttonRead.Text = "Read configuration";
            this.buttonRead.UseVisualStyleBackColor = true;
            this.buttonRead.Click += new System.EventHandler(this.button1_Click);
            // 
            // tableLayoutPokey
            // 
            this.tableLayoutPokey.AllowDrop = true;
            this.tableLayoutPokey.AutoScroll = true;
            this.tableLayoutPokey.AutoSize = true;
            this.tableLayoutPokey.ColumnCount = 4;
            this.tableLayoutPokey.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPokey.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPokey.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 380F));
            this.tableLayoutPokey.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPokey.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPokey.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPokey.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPokey.Controls.Add(this.label5, 3, 0);
            this.tableLayoutPokey.Location = new System.Drawing.Point(22, 33);
            this.tableLayoutPokey.Name = "tableLayoutPokey";
            this.tableLayoutPokey.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPokey.RowCount = 1;
            this.tableLayoutPokey.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayoutPokey.Size = new System.Drawing.Size(609, 200);
            this.tableLayoutPokey.TabIndex = 3;
            this.tableLayoutPokey.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPokey_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pin";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Etat";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(108, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Fonction";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(488, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Edit";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPokey);
            this.groupBox1.Location = new System.Drawing.Point(40, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(657, 264);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pokey Configuration";
            // 
            // progressBarBmsRunnning
            // 
            this.progressBarBmsRunnning.Location = new System.Drawing.Point(597, 363);
            this.progressBarBmsRunnning.Name = "progressBarBmsRunnning";
            this.progressBarBmsRunnning.Size = new System.Drawing.Size(100, 23);
            this.progressBarBmsRunnning.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(507, 366);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "F4 is running";
            // 
            // buttonTest
            // 
            this.buttonTest.Location = new System.Drawing.Point(70, 362);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(113, 23);
            this.buttonTest.TabIndex = 7;
            this.buttonTest.Text = "Test Key Commands";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttonLoadBMSKeystrokes
            // 
            this.buttonLoadBMSKeystrokes.Location = new System.Drawing.Point(271, 362);
            this.buttonLoadBMSKeystrokes.Name = "buttonLoadBMSKeystrokes";
            this.buttonLoadBMSKeystrokes.Size = new System.Drawing.Size(138, 23);
            this.buttonLoadBMSKeystrokes.TabIndex = 8;
            this.buttonLoadBMSKeystrokes.Text = "LoadKeyStroke BMS";
            this.buttonLoadBMSKeystrokes.UseVisualStyleBackColor = true;
            this.buttonLoadBMSKeystrokes.Click += new System.EventHandler(this.buttonLoadBMSKeystrokes_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 398);
            this.Controls.Add(this.buttonLoadBMSKeystrokes);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.progressBarBmsRunnning);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonRead);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selectListPokeys);
            this.Name = "Form1";
            this.Text = "PokeysToF4 - vBETA-1.2 - By Myoda";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPokey.ResumeLayout(false);
            this.tableLayoutPokey.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox selectListPokeys;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonRead;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPokey;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ProgressBar progressBarBmsRunnning;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.Button buttonLoadBMSKeystrokes;
    }
}

