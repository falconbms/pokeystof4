﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokeysToF4
{
    public static class IDictionaryExtensions
    {
        public static TKey FindKeyByValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TValue value)
        {
            if (dictionary == null)
                throw new ArgumentNullException("dictionary");

            foreach (KeyValuePair<TKey, TValue> pair in dictionary)
                if (value.Equals(pair.Value)) return pair.Key;

            throw new Exception("the value : " + value + " is not found in the dictionary");
        }
    }


    class loadBmsKeystrokes
    {
        utils Utils = new utils();

        public void loadSettings(Form callingForm, string lastSettingFile = null)
        {
            var mainForm = callingForm as Form1;
            string theFile = null;
            if (lastSettingFile != null)
            {
                if (File.Exists(lastSettingFile))
                {
                    theFile = lastSettingFile;
                }
                else utils.Displaynotify("File not exist ...");
            }
            else
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "KeyStrokes Files|*.key";
                openFileDialog1.Title = "Please select an BMS KeyStroke file to load ...";
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string configFileName = openFileDialog1.FileName;
                    if (File.Exists(configFileName))
                    {
                        theFile = configFileName;
                    }
                    else utils.Displaynotify("File not exist ...");

                }
                else utils.Displaynotify("Unable to load the file ...");
            }
            if (theFile != null)
            {
                FileStream read = new FileStream(theFile, FileMode.Open, FileAccess.Read, FileShare.Read);
                string fileContents;
                using (StreamReader reader = new StreamReader(read))
                {
                    fileContents = reader.ReadToEnd();
                }


                string[] lines = fileContents.Split(
                    new[] { Environment.NewLine },
                    StringSplitOptions.None
                );

                string Groupe = "Default";
                for(int i=0; i< lines.Length; i++)
                {
                    //Utils.Dbg(lines[i].ToString());
                    //Form1.importKeystroke.Add()
                    if(lines[i].StartsWith("SimDoNothing -1 0 0XFFFFFFFF 0 0 0 -1 ")) // cas d'un groupe.
                    {
                        string[] gp = lines[i].Split(new string[] { "SimDoNothing -1 0 0XFFFFFFFF 0 0 0 -1 " }, StringSplitOptions.None);
                        Regex rgx = new Regex("[^a-zA-Z]");
                        Groupe = rgx.Replace(gp[1], "");
                    }

                    if (!lines[i].StartsWith("#========================")) // cas d'un séparateur.
                    {
                        if(!lines[i].StartsWith("SimDoNothing"))
                        {
                            string[] global = lines[i].Split(new string[] { "\"" }, StringSplitOptions.None);
                            string[] step = lines[i].Split(new string[] { " " }, StringSplitOptions.None);
                            if (global.Length == 3)
                            {
                                //Utils.Dbg(Groupe + " => " + step[3] + " " + step[4] + " " + global[1]);

                                string panel = Groupe;
                                /*string keyStroke = Enum.GetName(typeof(PokeysToF4.Keyboard.DirectXKeyStrokes), step[3]);
                                bool modifierCtrl = Utils.getModifierBitMask(Convert.ToByte(Int32.Parse(step[4])), "Ctrl");
                                bool modifierShift = Utils.getModifierBitMask(Convert.ToByte(Int32.Parse(step[4])), "Shift");
                                bool modifierAlt = Utils.getModifierBitMask(Convert.ToByte(Int32.Parse(step[4])), "Alt");
                                bool modifierAltGr = Utils.getModifierBitMask(Convert.ToByte(Int32.Parse(step[4])), "AltGr");
                                string modifier = step[4];*/
                                string BMSFunction = step[0];
                                string BMSHelpText = global[1];

                                if(Convert.ToInt32(step[3], 16) != -1) // on a trouvé une correspondance
                                {
                                    keyCode kc = new keyCode();
                                    kc.modifierCtrl = Utils.getModifierDxBitMask(Convert.ToByte(Int32.Parse(step[4])), "Ctrl");
                                    kc.modifierShift = Utils.getModifierDxBitMask(Convert.ToByte(Int32.Parse(step[4])), "Shift");
                                    kc.modifierAlt = Utils.getModifierDxBitMask(Convert.ToByte(Int32.Parse(step[4])), "Alt");
                                    kc.modifierAltGr = Utils.getModifierDxBitMask(Convert.ToByte(Int32.Parse(step[4])), "AltGr");

                                    //pour obtenir la valeur sous forme de texte rajouter un tostring();
                                    //kc.key = Enum.ToObject(typeof(Keyboard.DirectXKeyStrokes), Convert.ToInt32(step[3], 16)).ToString();
                                    //récupération et transformation en keystroke DirectX
                                    var maKey = (Keyboard.DirectXKeyStrokes)Enum.ToObject(typeof(Keyboard.DirectXKeyStrokes), Convert.ToInt32(step[3], 16));
                                    //filtrages
                                    if (maKey == Keyboard.DirectXKeyStrokes.DIK_DECIMAL) maKey = Keyboard.DirectXKeyStrokes.DIK_PERIOD;
                                    if (maKey == Keyboard.DirectXKeyStrokes.DIK_SUBTRACT) maKey = Keyboard.DirectXKeyStrokes.DIK_MINUS;                            

                                    string result = Form1.PokeyDirectXKeybTranslation.FindKeyByValue(maKey);
                                    kc.key = result;
                                    string key = panel + " : " + BMSFunction + " (" + BMSHelpText + ")";
                                    if (!Form1.importKeystroke.ContainsKey(key)) Form1.importKeystroke.Add(key, kc);
                                }

                               
                            }

   
                            


                        }
                    }


                }

                //Utils.Dbg(lines[41].ToString());


            }
        }


    }
}
