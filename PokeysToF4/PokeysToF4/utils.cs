﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Threading;

namespace PokeysToF4
{
    
    class utils
    {

        public static StringBuilder logData = new StringBuilder();

        /* reconnaissance des KeyCode sur Pokey */
        public void loadListKeyCode()
        {
            FormPinsConfigure.comboBoxListKeyCode.Add("", "");
            FormPinsConfigure.comboBoxListKeyCode.Add("a", "0x4");
            FormPinsConfigure.comboBoxListKeyCode.Add("b", "0x5");
            FormPinsConfigure.comboBoxListKeyCode.Add("c", "0x6");
            FormPinsConfigure.comboBoxListKeyCode.Add("d", "0x7");
            FormPinsConfigure.comboBoxListKeyCode.Add("e", "0x8");
            FormPinsConfigure.comboBoxListKeyCode.Add("f", "0x9");
            FormPinsConfigure.comboBoxListKeyCode.Add("g", "0x0A");
            FormPinsConfigure.comboBoxListKeyCode.Add("h", "0x0B");
            FormPinsConfigure.comboBoxListKeyCode.Add("i", "0x0C");
            FormPinsConfigure.comboBoxListKeyCode.Add("j", "0x0D");
            FormPinsConfigure.comboBoxListKeyCode.Add("k", "0x0E");
            FormPinsConfigure.comboBoxListKeyCode.Add("l", "0x0F");
            FormPinsConfigure.comboBoxListKeyCode.Add("m", "0x10");
            FormPinsConfigure.comboBoxListKeyCode.Add("n", "0x11");
            FormPinsConfigure.comboBoxListKeyCode.Add("o", "0x12");
            FormPinsConfigure.comboBoxListKeyCode.Add("p", "0x13");
            FormPinsConfigure.comboBoxListKeyCode.Add("q", "0x14");
            FormPinsConfigure.comboBoxListKeyCode.Add("r", "0x15");
            FormPinsConfigure.comboBoxListKeyCode.Add("s", "0x16");
            FormPinsConfigure.comboBoxListKeyCode.Add("t", "0x17");
            FormPinsConfigure.comboBoxListKeyCode.Add("u", "0x18");
            FormPinsConfigure.comboBoxListKeyCode.Add("v", "0x19");
            FormPinsConfigure.comboBoxListKeyCode.Add("w", "0x1A");
            FormPinsConfigure.comboBoxListKeyCode.Add("x", "0x1B");
            FormPinsConfigure.comboBoxListKeyCode.Add("y", "0x1C");
            FormPinsConfigure.comboBoxListKeyCode.Add("z", "0x1D");
            FormPinsConfigure.comboBoxListKeyCode.Add("1", "0x1E");
            FormPinsConfigure.comboBoxListKeyCode.Add("2", "0x1F");
            FormPinsConfigure.comboBoxListKeyCode.Add("3", "0x20");
            FormPinsConfigure.comboBoxListKeyCode.Add("4", "0x21");
            FormPinsConfigure.comboBoxListKeyCode.Add("5", "0x22");
            FormPinsConfigure.comboBoxListKeyCode.Add("6", "0x23");
            FormPinsConfigure.comboBoxListKeyCode.Add("7", "0x24");
            FormPinsConfigure.comboBoxListKeyCode.Add("8", "0x25");
            FormPinsConfigure.comboBoxListKeyCode.Add("9", "0x26");
            FormPinsConfigure.comboBoxListKeyCode.Add("0", "0x27");
            FormPinsConfigure.comboBoxListKeyCode.Add("Minus", "0x2D");
            FormPinsConfigure.comboBoxListKeyCode.Add("Equal", "0x2E");
            FormPinsConfigure.comboBoxListKeyCode.Add("SquareOpeningBracket", "0x2F");
            FormPinsConfigure.comboBoxListKeyCode.Add("SquareClosingBracket", "0x30");
            FormPinsConfigure.comboBoxListKeyCode.Add("BackSlash", "0x31");
            FormPinsConfigure.comboBoxListKeyCode.Add("Semicolon", "0x33");
            FormPinsConfigure.comboBoxListKeyCode.Add("Apostrophe", "0x34");
            FormPinsConfigure.comboBoxListKeyCode.Add("Tilde", "0x35");
            FormPinsConfigure.comboBoxListKeyCode.Add("Comma", "0x36");
            FormPinsConfigure.comboBoxListKeyCode.Add("FullStop", "0x37");
            FormPinsConfigure.comboBoxListKeyCode.Add("Slash", "0x38");
            FormPinsConfigure.comboBoxListKeyCode.Add("Return", "0x28");
            FormPinsConfigure.comboBoxListKeyCode.Add("Escape", "0x29");
            FormPinsConfigure.comboBoxListKeyCode.Add("Backspace", "0x2A");
            FormPinsConfigure.comboBoxListKeyCode.Add("Tab", "0x2B");
            FormPinsConfigure.comboBoxListKeyCode.Add("Space", "0x2C");
            FormPinsConfigure.comboBoxListKeyCode.Add("CapsLock", "0x39");
            FormPinsConfigure.comboBoxListKeyCode.Add("F1", "0x3A");
            FormPinsConfigure.comboBoxListKeyCode.Add("F2", "0x3B");
            FormPinsConfigure.comboBoxListKeyCode.Add("F3", "0x3C");
            FormPinsConfigure.comboBoxListKeyCode.Add("F4", "0x3D");
            FormPinsConfigure.comboBoxListKeyCode.Add("F5", "0x3E");
            FormPinsConfigure.comboBoxListKeyCode.Add("F6", "0x3F");
            FormPinsConfigure.comboBoxListKeyCode.Add("F7", "0x40");
            FormPinsConfigure.comboBoxListKeyCode.Add("F8", "0x41");
            FormPinsConfigure.comboBoxListKeyCode.Add("F9", "0x42");
            FormPinsConfigure.comboBoxListKeyCode.Add("F10", "0x43");
            FormPinsConfigure.comboBoxListKeyCode.Add("F11", "0x44");
            FormPinsConfigure.comboBoxListKeyCode.Add("F12", "0x45");
            FormPinsConfigure.comboBoxListKeyCode.Add("PrintScreen", "0x46");
            FormPinsConfigure.comboBoxListKeyCode.Add("ScrollLock", "0x47");
            FormPinsConfigure.comboBoxListKeyCode.Add("Insert", "0x49");
            FormPinsConfigure.comboBoxListKeyCode.Add("Home", "0x4A");
            FormPinsConfigure.comboBoxListKeyCode.Add("PageUp", "0x4B");
            FormPinsConfigure.comboBoxListKeyCode.Add("Delete", "0x4C");
            FormPinsConfigure.comboBoxListKeyCode.Add("End", "0x4D");
            FormPinsConfigure.comboBoxListKeyCode.Add("PageDown", "0x4E");
            FormPinsConfigure.comboBoxListKeyCode.Add("RightArrow", "0x4F");
            FormPinsConfigure.comboBoxListKeyCode.Add("LeftArrow", "0x50");
            FormPinsConfigure.comboBoxListKeyCode.Add("DownArrow", "0x51");
            FormPinsConfigure.comboBoxListKeyCode.Add("UpArrow", "0x52");
            FormPinsConfigure.comboBoxListKeyCode.Add("NumLock", "0x53");
            FormPinsConfigure.comboBoxListKeyCode.Add("KeypadDivide", "0x54");
            FormPinsConfigure.comboBoxListKeyCode.Add("KeypadMultiply", "0x55");
            FormPinsConfigure.comboBoxListKeyCode.Add("KeypadMinus", "0x56");
            FormPinsConfigure.comboBoxListKeyCode.Add("KeypadPlus", "0x57");
            FormPinsConfigure.comboBoxListKeyCode.Add("KeypadEnter", "0x58");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad1", "0x59");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad2", "0x5A");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad3", "0x5B");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad4", "0x5C");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad5", "0x5D");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad6", "0x5E");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad7", "0x5F");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad8", "0x60");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad9", "0x61");
            FormPinsConfigure.comboBoxListKeyCode.Add("Keypad0", "0x62");
            FormPinsConfigure.comboBoxListKeyCode.Add("KeypadDot", "0x63");
        }

        /* reconnaissance des Keycode sur DirectX (voir classe Keyboard) */
        public void pokeyDirectXKeyTranslate()
        {
            Form1.PokeyDirectXKeybTranslation.Add("a", Keyboard.DirectXKeyStrokes.DIK_A);
            Form1.PokeyDirectXKeybTranslation.Add("b", Keyboard.DirectXKeyStrokes.DIK_B);
            Form1.PokeyDirectXKeybTranslation.Add("c", Keyboard.DirectXKeyStrokes.DIK_C);
            Form1.PokeyDirectXKeybTranslation.Add("d", Keyboard.DirectXKeyStrokes.DIK_D);
            Form1.PokeyDirectXKeybTranslation.Add("e", Keyboard.DirectXKeyStrokes.DIK_E);
            Form1.PokeyDirectXKeybTranslation.Add("f", Keyboard.DirectXKeyStrokes.DIK_F);
            Form1.PokeyDirectXKeybTranslation.Add("g", Keyboard.DirectXKeyStrokes.DIK_G);
            Form1.PokeyDirectXKeybTranslation.Add("h", Keyboard.DirectXKeyStrokes.DIK_H);
            Form1.PokeyDirectXKeybTranslation.Add("i", Keyboard.DirectXKeyStrokes.DIK_I);
            Form1.PokeyDirectXKeybTranslation.Add("j", Keyboard.DirectXKeyStrokes.DIK_J);   
            Form1.PokeyDirectXKeybTranslation.Add("k", Keyboard.DirectXKeyStrokes.DIK_K);
            Form1.PokeyDirectXKeybTranslation.Add("l", Keyboard.DirectXKeyStrokes.DIK_L);
            Form1.PokeyDirectXKeybTranslation.Add("m", Keyboard.DirectXKeyStrokes.DIK_M);
            Form1.PokeyDirectXKeybTranslation.Add("n", Keyboard.DirectXKeyStrokes.DIK_N);
            Form1.PokeyDirectXKeybTranslation.Add("o", Keyboard.DirectXKeyStrokes.DIK_O);
            Form1.PokeyDirectXKeybTranslation.Add("p", Keyboard.DirectXKeyStrokes.DIK_P);
            Form1.PokeyDirectXKeybTranslation.Add("q", Keyboard.DirectXKeyStrokes.DIK_Q);
            Form1.PokeyDirectXKeybTranslation.Add("r", Keyboard.DirectXKeyStrokes.DIK_R);
            Form1.PokeyDirectXKeybTranslation.Add("s", Keyboard.DirectXKeyStrokes.DIK_S);
            Form1.PokeyDirectXKeybTranslation.Add("t", Keyboard.DirectXKeyStrokes.DIK_T);
            Form1.PokeyDirectXKeybTranslation.Add("u", Keyboard.DirectXKeyStrokes.DIK_U);
            Form1.PokeyDirectXKeybTranslation.Add("v", Keyboard.DirectXKeyStrokes.DIK_V);
            Form1.PokeyDirectXKeybTranslation.Add("w", Keyboard.DirectXKeyStrokes.DIK_W);
            Form1.PokeyDirectXKeybTranslation.Add("x", Keyboard.DirectXKeyStrokes.DIK_X);
            Form1.PokeyDirectXKeybTranslation.Add("y", Keyboard.DirectXKeyStrokes.DIK_Y);
            Form1.PokeyDirectXKeybTranslation.Add("z", Keyboard.DirectXKeyStrokes.DIK_Z);
            Form1.PokeyDirectXKeybTranslation.Add("1", Keyboard.DirectXKeyStrokes.DIK_1);
            Form1.PokeyDirectXKeybTranslation.Add("2", Keyboard.DirectXKeyStrokes.DIK_2);
            Form1.PokeyDirectXKeybTranslation.Add("3", Keyboard.DirectXKeyStrokes.DIK_3);
            Form1.PokeyDirectXKeybTranslation.Add("4", Keyboard.DirectXKeyStrokes.DIK_4);
            Form1.PokeyDirectXKeybTranslation.Add("5", Keyboard.DirectXKeyStrokes.DIK_5);
            Form1.PokeyDirectXKeybTranslation.Add("6", Keyboard.DirectXKeyStrokes.DIK_6);
            Form1.PokeyDirectXKeybTranslation.Add("7", Keyboard.DirectXKeyStrokes.DIK_7);
            Form1.PokeyDirectXKeybTranslation.Add("8", Keyboard.DirectXKeyStrokes.DIK_8);
            Form1.PokeyDirectXKeybTranslation.Add("9", Keyboard.DirectXKeyStrokes.DIK_9);
            Form1.PokeyDirectXKeybTranslation.Add("0", Keyboard.DirectXKeyStrokes.DIK_0);
            Form1.PokeyDirectXKeybTranslation.Add("Minus", Keyboard.DirectXKeyStrokes.DIK_MINUS);
            Form1.PokeyDirectXKeybTranslation.Add("Equal", Keyboard.DirectXKeyStrokes.DIK_EQUALS);
            Form1.PokeyDirectXKeybTranslation.Add("SquareOpeningBracket", Keyboard.DirectXKeyStrokes.DIK_LBRACKET);
            Form1.PokeyDirectXKeybTranslation.Add("SquareClosingBracket", Keyboard.DirectXKeyStrokes.DIK_RBRACKET);
            Form1.PokeyDirectXKeybTranslation.Add("BackSlash", Keyboard.DirectXKeyStrokes.DIK_BACKSLASH);
            Form1.PokeyDirectXKeybTranslation.Add("Semicolon", Keyboard.DirectXKeyStrokes.DIK_SEMICOLON);
            Form1.PokeyDirectXKeybTranslation.Add("Apostrophe", Keyboard.DirectXKeyStrokes.DIK_APOSTROPHE);
            Form1.PokeyDirectXKeybTranslation.Add("Tilde", Keyboard.DirectXKeyStrokes.DIK_GRAVE);
            Form1.PokeyDirectXKeybTranslation.Add("Comma", Keyboard.DirectXKeyStrokes.DIK_COMMA);
            Form1.PokeyDirectXKeybTranslation.Add("FullStop", Keyboard.DirectXKeyStrokes.DIK_STOP);
            Form1.PokeyDirectXKeybTranslation.Add("Slash", Keyboard.DirectXKeyStrokes.DIK_SLASH);
            Form1.PokeyDirectXKeybTranslation.Add("Return", Keyboard.DirectXKeyStrokes.DIK_RETURN);
            Form1.PokeyDirectXKeybTranslation.Add("Escape", Keyboard.DirectXKeyStrokes.DIK_ESCAPE);
            Form1.PokeyDirectXKeybTranslation.Add("Backspace", Keyboard.DirectXKeyStrokes.DIK_BACK);
            Form1.PokeyDirectXKeybTranslation.Add("Tab", Keyboard.DirectXKeyStrokes.DIK_TAB);
            Form1.PokeyDirectXKeybTranslation.Add("Space", Keyboard.DirectXKeyStrokes.DIK_SPACE);
            Form1.PokeyDirectXKeybTranslation.Add("CapsLock", Keyboard.DirectXKeyStrokes.DIK_CAPSLOCK);
            Form1.PokeyDirectXKeybTranslation.Add("F1", Keyboard.DirectXKeyStrokes.DIK_F1);
            Form1.PokeyDirectXKeybTranslation.Add("F2", Keyboard.DirectXKeyStrokes.DIK_F2);
            Form1.PokeyDirectXKeybTranslation.Add("F3", Keyboard.DirectXKeyStrokes.DIK_F3);
            Form1.PokeyDirectXKeybTranslation.Add("F4", Keyboard.DirectXKeyStrokes.DIK_F4);
            Form1.PokeyDirectXKeybTranslation.Add("F5", Keyboard.DirectXKeyStrokes.DIK_F5);
            Form1.PokeyDirectXKeybTranslation.Add("F6", Keyboard.DirectXKeyStrokes.DIK_F6);
            Form1.PokeyDirectXKeybTranslation.Add("F7", Keyboard.DirectXKeyStrokes.DIK_F7);
            Form1.PokeyDirectXKeybTranslation.Add("F8", Keyboard.DirectXKeyStrokes.DIK_F8);
            Form1.PokeyDirectXKeybTranslation.Add("F9", Keyboard.DirectXKeyStrokes.DIK_F9);
            Form1.PokeyDirectXKeybTranslation.Add("F10", Keyboard.DirectXKeyStrokes.DIK_F10);
            Form1.PokeyDirectXKeybTranslation.Add("F11", Keyboard.DirectXKeyStrokes.DIK_F11);
            Form1.PokeyDirectXKeybTranslation.Add("F12", Keyboard.DirectXKeyStrokes.DIK_F12);
            Form1.PokeyDirectXKeybTranslation.Add("PrintScreen", Keyboard.DirectXKeyStrokes.DIK_SYSRQ);
            Form1.PokeyDirectXKeybTranslation.Add("ScrollLock", Keyboard.DirectXKeyStrokes.DIK_SCROLL);
            Form1.PokeyDirectXKeybTranslation.Add("Insert", Keyboard.DirectXKeyStrokes.DIK_INSERT);
            Form1.PokeyDirectXKeybTranslation.Add("Home", Keyboard.DirectXKeyStrokes.DIK_HOME);
            Form1.PokeyDirectXKeybTranslation.Add("PageUp", Keyboard.DirectXKeyStrokes.DIK_PGUP);
            Form1.PokeyDirectXKeybTranslation.Add("Delete", Keyboard.DirectXKeyStrokes.DIK_DELETE);
            Form1.PokeyDirectXKeybTranslation.Add("End", Keyboard.DirectXKeyStrokes.DIK_END);
            Form1.PokeyDirectXKeybTranslation.Add("PageDown", Keyboard.DirectXKeyStrokes.DIK_PGDN);
            Form1.PokeyDirectXKeybTranslation.Add("RightArrow", Keyboard.DirectXKeyStrokes.DIK_RIGHTARROW);
            Form1.PokeyDirectXKeybTranslation.Add("LeftArrow", Keyboard.DirectXKeyStrokes.DIK_LEFTARROW);
            Form1.PokeyDirectXKeybTranslation.Add("DownArrow", Keyboard.DirectXKeyStrokes.DIK_DOWNARROW);
            Form1.PokeyDirectXKeybTranslation.Add("UpArrow", Keyboard.DirectXKeyStrokes.DIK_UPARROW);
            Form1.PokeyDirectXKeybTranslation.Add("NumLock", Keyboard.DirectXKeyStrokes.DIK_NUMLOCK);
            Form1.PokeyDirectXKeybTranslation.Add("KeypadDivide", Keyboard.DirectXKeyStrokes.DIK_DIVIDE);
            Form1.PokeyDirectXKeybTranslation.Add("KeypadMultiply", Keyboard.DirectXKeyStrokes.DIK_MULTIPLY);
            Form1.PokeyDirectXKeybTranslation.Add("KeypadMinus", Keyboard.DirectXKeyStrokes.DIK_MINUS);
            Form1.PokeyDirectXKeybTranslation.Add("KeypadPlus", Keyboard.DirectXKeyStrokes.DIK_ADD);
            Form1.PokeyDirectXKeybTranslation.Add("KeypadEnter", Keyboard.DirectXKeyStrokes.DIK_NUMPADENTER);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad1", Keyboard.DirectXKeyStrokes.DIK_NUMPAD1);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad2", Keyboard.DirectXKeyStrokes.DIK_NUMPAD2);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad3", Keyboard.DirectXKeyStrokes.DIK_NUMPAD3);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad4", Keyboard.DirectXKeyStrokes.DIK_NUMPAD4);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad5", Keyboard.DirectXKeyStrokes.DIK_NUMPAD5);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad6", Keyboard.DirectXKeyStrokes.DIK_NUMPAD6);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad7", Keyboard.DirectXKeyStrokes.DIK_NUMPAD7);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad8", Keyboard.DirectXKeyStrokes.DIK_NUMPAD8);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad9", Keyboard.DirectXKeyStrokes.DIK_NUMPAD9);
            Form1.PokeyDirectXKeybTranslation.Add("Keypad0", Keyboard.DirectXKeyStrokes.DIK_NUMPAD0);
            Form1.PokeyDirectXKeybTranslation.Add("KeypadDot", Keyboard.DirectXKeyStrokes.DIK_PERIOD);
        }

        /* Fonction pour afficher les erreurs dans la console  */
        public void Dbg(string str)
        {
            logData.Append(DateTime.Now.ToString("dd/MM/yyyy H:mm:ss tt") + "\t" + str + "\r\n");
            try
            {
                Console.WriteLine(str);
            }
            catch (Exception ex)
            {

            }
        }



        public void onStartdetectVersion()
        {
            if (checkingLastVersion())
            {
                Dbg("You got the last version !");
            }
        }



        /* show notifications */
        public static void Displaynotify(string Text)
        {
            MessageBox.Show(Text, "Warning ! ");
        }



        public bool checkingLastVersion()
        {
            Dbg("Checking app version");
            string url = "https://f4toserial.com/PokeysToF4Version.txt";
            var webRequest = WebRequest.Create(@"" + url);
            try
            {
                using (var response = webRequest.GetResponse())
                using (var content = response.GetResponseStream())
                using (var reader = new StreamReader(content))
                {
                    //programVersion
                    string strContent = reader.ReadToEnd();
                    if (strContent != Form1.programVersion)
                    {
                        Dbg("A new version is available !");
                        MessageBox.Show("A new version is available ! Please check on https://f4toserial.com", "Warning ! ");

                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                Form1.checkVersion.Abort();
            }
            catch (Exception ex)
            {
                Dbg("Unable to contact the server http://f4toserial.com/");
                return false;
            }
            return false;
        }





        /* récupère la liste des cartes pokeys connectées en USB sur l'ordinateur */
        public void listPokeys()
        {
            Dbg("PoKeys device enumeration\n");
            // Create PoKeys55 device object
            PoKeysDevice_DLL.PoKeysDevice device = new PoKeysDevice_DLL.PoKeysDevice();

            // Enumerate devices (this step must never be ommited!)
            int iNumDevices = device.EnumerateDevices();

            Dbg("Found " + iNumDevices + " PoKeys devices");


            // Connect to every device and list data
            for (int i = 0; i < iNumDevices; i++)
            {
                device.ConnectToDevice(i);

                byte userID = 0;
                int serialNumber = 0;
                int versionMajor = 0;
                int versionMinor = 0;
                string deviceName = "";

                device.GetUserID(ref userID);
                device.GetUserDeviceName(ref deviceName);
                device.GetDeviceIDEx(ref serialNumber, ref versionMajor, ref versionMinor);

                pokey p = new pokey();
                p.id = i;
                p.userId = userID;
                p.sn = serialNumber;
                p.versionMajor = versionMajor;
                p.versionMinor = versionMinor;
                p.deviceName = deviceName;

                Form1.pokeysList.Add(i, p);

                device.DisconnectDevice();
            }

        }



        /* recheche un code modifier parmis les resultat envoyé */
        public bool getModifierBitMask(byte PokeyModifiers,string Mask)
        {
            keyModifiers KeyTest = (keyModifiers)PokeyModifiers;

            switch (Mask)
            {
                case "Ctrl":
                    return (KeyTest & keyModifiers.CtrlMask) != 0;
                case "Shift":
                    return (KeyTest & keyModifiers.ShiftMask) != 0;
                case "Alt":
                    return (KeyTest & keyModifiers.AltMask) != 0;
                case "Win":
                    return (KeyTest & keyModifiers.WinMask) != 0;
                case "AltGr":
                    return (KeyTest & keyModifiers.AltGrMask) != 0;
            }
            return false;
        }


        /* recheche un code modifier parmis les resultat envoyé */
        public bool getModifierDxBitMask(byte PokeyModifiers, string Mask)
        {
            Keyboard.keyModifiers KeyTest = (Keyboard.keyModifiers)PokeyModifiers;

            switch (Mask)
            {
                case "Ctrl":
                    return (KeyTest & Keyboard.keyModifiers.CtrlMask) != 0;
                case "Shift":
                    return (KeyTest & Keyboard.keyModifiers.ShiftMask) != 0;
                case "Alt":
                    return (KeyTest & Keyboard.keyModifiers.AltMask) != 0;
            }
            return false;
        }


        /* Cette methode envoie les touches clavier à Falcon BMS en fonction des valeurs configurées */

        public async void sendAllKeyCommandToF4()
        { 

            Dbg(" --------------- Will run after 3 sec ---------------");
            await Task.Delay(TimeSpan.FromSeconds(3)); //execute la command après 3 secondes !

            foreach (KeyValuePair<int, SortedList<int, pokeyPinConfig>> KVpokey in Form1.pokeyPinConfig_list)
            {
                int pokeyID = KVpokey.Key;
                SortedList<int, pokeyPinConfig> pConfig = KVpokey.Value;

                
                PoKeysDevice_DLL.PoKeysDevice device = new PoKeysDevice_DLL.PoKeysDevice();
                // Enumerate devices (this step must never be ommited!)
                int iNumDevices = device.EnumerateDevices();
                if (iNumDevices == 0)
                {
                    Dbg("No PoKeys55 devices found!");
                    Dbg("\nPress any key to exit");
                    return;
                }
                // Connect to first PoKeys55 device
                device.ConnectToDevice(pokeyID);
                Dbg("Connect to Pokey " + pokeyID);

                foreach (KeyValuePair<int, pokeyPinConfig> config in pConfig)
                {

                    pokeyPinConfig pin = config.Value;

                    if (pin.etat == false) // cas ou la bascule du switch est connectée.
                    {
                        if (pin.keyCodeOff != 0)
                        {
                            string keyToSend = pin.keyCodeOff.ToString().Replace("key_", "");
                            Dbg("Send Pokey :" + pokeyID + " Pin : " + pin.pin + " KeyCode " + keyToSend);

                            // dans le cas d'un switch ON-OFF-ON, on vas chécker les infos du pin suivant 
                            if(pin.linkedTo != -1)
                            {
                                Dbg("Pin linked avec un autre !");
                                var nextPin = Form1.pokeyPinConfig_list[pokeyID][pin.linkedTo];

                                if (nextPin.etat == true) {
                                    Dbg("Next Pin etat = true, on execute touche " + keyToSend);
                                    sendKey(keyToSend, pin.modifierCtrlOff, pin.modifierShitOff, pin.modifierAltOff, pin.modifierAltGrOff); }

                            }
                            else
                            {
                                sendKey(keyToSend, pin.modifierCtrlOff, pin.modifierShitOff, pin.modifierAltOff, pin.modifierAltGrOff);
                            }
                        }                        
                    }
                    else // Cas par défaut ! Pin non connecté ou bascule non activé sur le pin.
                    {
                        if (pin.keyCodeOn != 0)
                        {

                            string keyToSend = pin.keyCodeOn.ToString().Replace("key_", "");
                            Dbg("Send Pokey :" + pokeyID + " Pin : " + pin.pin + " KeyCode " + keyToSend);

                            if (pin.linkedTo == -1)
                            {
                                Dbg("Pin non linké ! On execute touche : " + keyToSend);
                                sendKey(keyToSend, pin.modifierCtrlOn, pin.modifierShitOn, pin.modifierAltOn, pin.modifierAltGrOn);
                            }
                            else
                            {
                                var nextPin = Form1.pokeyPinConfig_list[pokeyID][pin.linkedTo];
                                if(nextPin.etat == true) sendKey(keyToSend, pin.modifierCtrlOn, pin.modifierShitOn, pin.modifierAltOn, pin.modifierAltGrOn);
                            }
                            
                        }
                    }

                    // waiting for 100 miliseconds to send next key command.
                    Thread.Sleep(50);
                }


                device.DisconnectDevice();

            }

        }



 
    



    private void sendKey(string Key, bool modifierCtrl, bool modifierShift, bool modifierAlt, bool modifierAltGr)
        {
            if (!Form1.PokeyDirectXKeybTranslation.ContainsKey(Key)) { Dbg("aucune correspondance de touche ! EXIT !");  return; }

            Dbg("===>> Send Command Key " + Key);

            /* Modifiers touche appuyée */
            if(modifierCtrl) Keyboard.SendKey(Keyboard.DirectXKeyStrokes.DIK_LCONTROL, false, Keyboard.InputType.Keyboard);
            if(modifierShift) Keyboard.SendKey(Keyboard.DirectXKeyStrokes.DIK_LCONTROL, false, Keyboard.InputType.Keyboard);
            if(modifierAlt) Keyboard.SendKey(Keyboard.DirectXKeyStrokes.DIK_LALT, false, Keyboard.InputType.Keyboard);
            if(modifierAltGr) Keyboard.SendKey(Keyboard.DirectXKeyStrokes.DIK_RALT, false, Keyboard.InputType.Keyboard);

            /* touche appuyée */
            Keyboard.SendKey(Form1.PokeyDirectXKeybTranslation[Key], false,Keyboard.InputType.Keyboard);
            /* touche remontée  */
            Keyboard.SendKey(Form1.PokeyDirectXKeybTranslation[Key], true, Keyboard.InputType.Keyboard);

            /* modifiers touche remontée */
            if (modifierCtrl) Keyboard.SendKey(Keyboard.DirectXKeyStrokes.DIK_LCONTROL, true, Keyboard.InputType.Keyboard);
            if (modifierShift) Keyboard.SendKey(Keyboard.DirectXKeyStrokes.DIK_LCONTROL, true, Keyboard.InputType.Keyboard);
            if (modifierAlt) Keyboard.SendKey(Keyboard.DirectXKeyStrokes.DIK_LALT, true, Keyboard.InputType.Keyboard);
            if (modifierAltGr) Keyboard.SendKey(Keyboard.DirectXKeyStrokes.DIK_RALT, true, Keyboard.InputType.Keyboard);
        }



        public void GetAllPokeysConfiguration()
        {

            foreach (var i in Form1.pokeysList)
            {
                getPokeysConfiguration(i.Value.id);
            }
            
            return;
        }



        /* cette methode parcourt la configuration d'une carte pokey et remplis le tableau pokeyPinConfig_list. */
        public void getPokeysConfiguration( int pokeyId)
        {
            if (pokeyId == -1) return;

            
            //Form1.pokeyPinConfig_list.Remove(pokeyId); // suppression des anciennes valeurs possible sur cette pokey
            
            // Create PoKeys55 device object
            PoKeysDevice_DLL.PoKeysDevice device = new PoKeysDevice_DLL.PoKeysDevice();


            // Enumerate devices (this step must never be ommited!)
            int iNumDevices = device.EnumerateDevices();

            if (iNumDevices == 0)
            {
                Dbg("No PoKeys55 devices found!");
                Dbg("\nPress any key to exit");
                return;
            }

            // Connect to first PoKeys55 device
            device.ConnectToDevice(pokeyId);
            
    

            Dbg("\nReading pin states:");

            



            SortedList<int, pokeyPinConfig> pinsConfig = new SortedList<int, pokeyPinConfig>();
            if (!Form1.pokeyPinConfig_list.ContainsKey(pokeyId)) Form1.pokeyPinConfig_list.Add(pokeyId, pinsConfig);


            bool[] pinState = new bool[55];
            byte[] ArrayKeyCodesOn = new byte[55];
            byte[] ArrayKeyCodesOff = new byte[55];
            byte[] ArrayModifiersOn = new byte[55];
            byte[] ArrayModifiersOff = new byte[55];

            device.BlockGetInputAll55(ref pinState);
            device.GetTriggeredInputMapping(ref ArrayKeyCodesOn, ref ArrayModifiersOn, ref ArrayKeyCodesOff, ref ArrayModifiersOff);

            for (byte i = 0; i < 55; i++)
            {
                // création d'une nouvelle liste de configuration pin / pin. 
                pokeyPinConfig p = new pokeyPinConfig();
                p.id = pokeyId;
                p.pin = i;
                p.etat = pinState[i];
                
                
                if (!Form1.pokeyPinConfig_list[pokeyId].ContainsKey(i)) Form1.pokeyPinConfig_list[pokeyId].Add(i,p);

                //ekeyCodes EnumArrayKeyCodesOn = (ekeyCodes)ArrayKeyCodesOn[i]; //retourne la clé de ekeyCodes par la valeur.

                Form1.pokeyPinConfig_list[pokeyId][i].etat = pinState[i];


                Form1.pokeyPinConfig_list[pokeyId][i].keyCodeOn = (ekeyCodes)ArrayKeyCodesOn[i]; //retourne la clé de ekeyCodes par la valeur.
                Form1.pokeyPinConfig_list[pokeyId][i].keyCodeOff = (ekeyCodes)ArrayKeyCodesOff[i]; //retourne la clé de ekeyCodes par la valeur.


                


                Form1.pokeyPinConfig_list[pokeyId][i].modifierCtrlOn = getModifierBitMask(ArrayModifiersOn[i], "Ctrl");
                Form1.pokeyPinConfig_list[pokeyId][i].modifierShitOn = getModifierBitMask(ArrayModifiersOn[i], "Shift");
                Form1.pokeyPinConfig_list[pokeyId][i].modifierAltOn = getModifierBitMask(ArrayModifiersOn[i], "Alt");
                Form1.pokeyPinConfig_list[pokeyId][i].modifierAltGrOn = getModifierBitMask(ArrayModifiersOn[i], "AltGr");

                Form1.pokeyPinConfig_list[pokeyId][i].modifierCtrlOff = getModifierBitMask(ArrayModifiersOff[i], "Ctrl");
                Form1.pokeyPinConfig_list[pokeyId][i].modifierShitOff = getModifierBitMask(ArrayModifiersOff[i], "Shift");
                Form1.pokeyPinConfig_list[pokeyId][i].modifierAltOff = getModifierBitMask(ArrayModifiersOff[i], "Alt");
                Form1.pokeyPinConfig_list[pokeyId][i].modifierAltGrOff = getModifierBitMask(ArrayModifiersOff[i], "AltGr");


                //pinsConfig.Add(i, p);       //ajoute un élément a la liste de config       
                //); //ajoute la config a la liste des pokeys.
            }


                //Form1.pokeyPinConfig_list.Add(pokeyId, pinsConfig); //ajoute la config a la liste des pokeys.

            // Disconnect from 
            device.DisconnectDevice();
        }





        public bool setPinConfig(byte pokeyId, byte pinId, string keyCodeOn, bool modifierCtrlOn, bool modifierShiftOn, bool modifierAltOn, bool modifierAltGrOn, string keyCodeOff, bool modifierCtrlOff, bool modifierShiftOff, bool modifierAltOff, bool modifierAltGrOff, int LinkedToPin)
        {

            if ((int)pokeyId == -1) return false;
          

            // création d'une nouvelle liste de configuration pin / pin. 
            pokeyPinConfig p = new pokeyPinConfig();
            p.id = pokeyId;
            p.pin = pinId;
            //récupération du keycode dynamiquement
            if (keyCodeOn != "") p.keyCodeOn = (ekeyCodes)System.Enum.Parse(typeof(ekeyCodes), "key_" + keyCodeOn);
            if (keyCodeOff != "") p.keyCodeOff = (ekeyCodes)System.Enum.Parse(typeof(ekeyCodes), "key_" + keyCodeOff);
            p.modifierCtrlOn = modifierCtrlOn; 
            p.modifierShitOn = modifierShiftOn;
            p.modifierAltOn = modifierAltOn;
            p.modifierAltGrOn = modifierAltGrOn;
            p.modifierCtrlOff = modifierCtrlOff;
            p.modifierShitOff = modifierShiftOff;
            p.modifierAltOff = modifierAltOff;
            p.modifierAltGrOff = modifierAltGrOff;
            p.linkedTo = LinkedToPin;

            Form1.pokeyPinConfig_list[pokeyId][pinId] = p; //ajoute la config a la liste des pokeys.


            Dbg("Pin " + (pinId + 1) + " of pokey" + pokeyId +  " saved !");


            return true;

        }


        /* mettre à jour la configuration d'une carte pokey */
        public void updatePokeyPinConfiguration()
        {
            Dbg("PoKeys device enumeration\n");
            // Create PoKeys55 device object
            PoKeysDevice_DLL.PoKeysDevice device = new PoKeysDevice_DLL.PoKeysDevice();

            // Enumerate devices (this step must never be ommited!)
            int iNumDevices = device.EnumerateDevices();

            Dbg("Found " + iNumDevices + " PoKeys devices");


            device.ConnectToDevice(Form1.currentPokeyConfigure);

            Byte[] ArrayKeyCodesOn = new Byte[55];
            Byte[] ArrayKeyCodesOff = new Byte[55];
            Byte[] ArrayNull = new Byte[55];
            Byte[] ArrayModifiersOn = new Byte[55];
            Byte[] ArrayModifiersOff = new Byte[55];
            Byte[] ArrayActualPinConfiguration = new Byte[56];// ca pete à 55, franchement, me demandez pas pkoi ... merci pokeys les branks ...  !  Dans la doc : Array of 55 bytes (for details, see SetPinData function)

            device.GetAllPinConfiguration(ref ArrayActualPinConfiguration); // Dans le cas d'un trigger input, on touche autrement on trouche pas ! Il faut coder ca !

            
            if (Form1.pokeyPinConfig_list.ContainsKey(Form1.currentPokeyConfigure))
            {
                foreach (KeyValuePair<int, pokeyPinConfig> kvp in Form1.pokeyPinConfig_list[Form1.currentPokeyConfigure])
                {
                    int pin = kvp.Key;
                    ArrayKeyCodesOn[pin] = (byte) kvp.Value.keyCodeOn;
                    ArrayKeyCodesOff[pin] = (byte)kvp.Value.keyCodeOff;



                    if ((byte)kvp.Value.keyCodeOn != 0)
                    {
                        if (kvp.Value.modifierCtrlOn) ArrayModifiersOn[pin] += (byte)keyModifiers.CtrlMask;
                        if (kvp.Value.modifierShitOn) ArrayModifiersOn[pin] += (byte)keyModifiers.ShiftMask;
                        if (kvp.Value.modifierAltOn) ArrayModifiersOn[pin] += (byte)keyModifiers.AltMask;
                        if (kvp.Value.modifierAltGrOn) ArrayModifiersOn[pin] += (byte)keyModifiers.AltGrMask;
                    }

                    if ((byte)kvp.Value.keyCodeOff != 0)
                    {
                        if (kvp.Value.modifierCtrlOff) ArrayModifiersOff[pin] += (byte)keyModifiers.CtrlMask;
                        if (kvp.Value.modifierShitOff) ArrayModifiersOff[pin] += (byte)keyModifiers.ShiftMask;
                        if (kvp.Value.modifierAltOff) ArrayModifiersOff[pin] += (byte)keyModifiers.AltMask;
                        if (kvp.Value.modifierAltGrOff) ArrayModifiersOff[pin] += (byte)keyModifiers.AltGrMask;
                    }

                    if ((byte)kvp.Value.keyCodeOn != 0 && (byte)kvp.Value.keyCodeOff != 0) // si on choisi des donnée (keycodes) alors un modifier pour trigger
                        device.SetPinData((byte)pin, (byte)(pinFunctionsEnum.pinFunctionTriggeredInput));
                    else device.SetPinData((byte)pin, (byte)(ArrayActualPinConfiguration[pin])); // autrement on garde l'ancienne configuration

                    


                }

            }
            

            device.SetTriggeredInputMapping(ref ArrayKeyCodesOn, ref ArrayModifiersOn, ref ArrayKeyCodesOff, ref ArrayModifiersOff);



            device.SaveConfiguration();
            device.DisconnectDevice();
                        
            //mise à jour des pokeys 
            GetAllPokeysConfiguration();
        }






    }
}
