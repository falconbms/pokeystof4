﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Runtime.InteropServices;
using System.Net;
using Newtonsoft.Json;
using F4SharedMem;
using System.Diagnostics;
using static PokeysToF4.Keyboard;

namespace PokeysToF4
{
    #region Enums
    // Key codes
    public enum ekeyCodes : byte
    {
        key_a = 0x4, // Shift combination: A
        key_b = 0x5, // Shift combination: B
        key_c = 0x6, // Shift combination: C
        key_d = 0x7, // Shift combination: D
        key_e = 0x8, // Shift combination: E
        key_f = 0x9, // Shift combination: F
        key_g = 0x0A, // Shift combination: G
        key_h = 0x0B, // Shift combination: H
        key_i = 0x0C, // Shift combination: I
        key_j = 0x0D, // Shift combination: J
        key_k = 0x0E, // Shift combination: K
        key_l = 0x0F, // Shift combination: L
        key_m = 0x10, // Shift combination: M
        key_n = 0x11, // Shift combination: N
        key_o = 0x12, // Shift combination: O
        key_p = 0x13, // Shift combination: P
        key_q = 0x14, // Shift combination: Q
        key_r = 0x15, // Shift combination: R
        key_s = 0x16, // Shift combination: S
        key_t = 0x17, // Shift combination: T
        key_u = 0x18, // Shift combination: U
        key_v = 0x19, // Shift combination: V
        key_w = 0x1A, // Shift combination: W
        key_x = 0x1B, // Shift combination: X
        key_y = 0x1C, // Shift combination: Y
        key_z = 0x1D, // Shift combination: Z
        key_1 = 0x1E, // Shift combination: !
        key_2 = 0x1F, // Shift combination: @
        key_3 = 0x20, // Shift combination: #
        key_4 = 0x21, // Shift combination: $
        key_5 = 0x22, // Shift combination: %
        key_6 = 0x23, // Shift combination: ^
        key_7 = 0x24, // Shift combination: &
        key_8 = 0x25, // Shift combination: *
        key_9 = 0x26, // Shift combination: (
        key_0 = 0x27, // Shift combination: )
        key_Minus = 0x2D, // Shift combination: _
        key_Equal = 0x2E, // Shift combination: +
        key_SquareOpeningBracket = 0x2F, // Shift combination: {
        key_SquareClosingBracket = 0x30, // Shift combination: }
        key_BackSlash = 0x31, // Shift combination: |
        key_Semicolon = 0x33, // Shift combination: :
        key_Apostrophe = 0x34, // Shift combination: 
        key_Tilde = 0x35, // Shift combination: ~
        key_Comma = 0x36, // Shift combination: <
        key_FullStop = 0x37, // Shift combination: >
        key_Slash = 0x38, // Shift combination: ?
        key_Return = 0x28, // Shift combination: 
        key_Escape = 0x29, // Shift combination: 
        key_Backspace = 0x2A, // Shift combination: 
        key_Tab = 0x2B, // Shift combination: 
        key_Space = 0x2C, // Shift combination: 
        key_CapsLock = 0x39, // Shift combination: 
        key_F1 = 0x3A, // Shift combination: 
        key_F2 = 0x3B, // Shift combination: 
        key_F3 = 0x3C, // Shift combination: 
        key_F4 = 0x3D, // Shift combination: 
        key_F5 = 0x3E, // Shift combination: 
        key_F6 = 0x3F, // Shift combination: 
        key_F7 = 0x40, // Shift combination: 
        key_F8 = 0x41, // Shift combination: 
        key_F9 = 0x42, // Shift combination: 
        key_F10 = 0x43, // Shift combination: 
        key_F11 = 0x44, // Shift combination: 
        key_F12 = 0x45, // Shift combination: 
        key_PrintScreen = 0x46, // Shift combination: 
        key_ScrollLock = 0x47, // Shift combination: 
        key_Insert = 0x49, // Shift combination: 
        key_Home = 0x4A, // Shift combination: 
        key_PageUp = 0x4B, // Shift combination: 
        key_Delete = 0x4C, // Shift combination: 
        key_End = 0x4D, // Shift combination: 
        key_PageDown = 0x4E, // Shift combination: 
        key_RightArrow = 0x4F, // Shift combination: 
        key_LeftArrow = 0x50, // Shift combination: 
        key_DownArrow = 0x51, // Shift combination: 
        key_UpArrow = 0x52, // Shift combination: 
        key_NumLock = 0x53, // Shift combination: 
        key_KeypadDivide = 0x54, // Shift combination: 
        key_KeypadMultiply = 0x55, // Shift combination: 
        key_KeypadMinus = 0x56, // Shift combination: 
        key_KeypadPlus = 0x57, // Shift combination: 
        key_KeypadEnter = 0x58, // Shift combination: 
        key_Keypad1 = 0x59, // Shift combination: 
        key_Keypad2 = 0x5A, // Shift combination: 
        key_Keypad3 = 0x5B, // Shift combination: 
        key_Keypad4 = 0x5C, // Shift combination: 
        key_Keypad5 = 0x5D, // Shift combination: 
        key_Keypad6 = 0x5E, // Shift combination: 
        key_Keypad7 = 0x5F, // Shift combination: 
        key_Keypad8 = 0x60, // Shift combination: 
        key_Keypad9 = 0x61, // Shift combination: 
        key_Keypad0 = 0x62, // Shift combination: 
        key_KeypadDot = 0x63 // Shift combination: 
    }

    public enum pinFunctionsEnum
    {
        pinFunctionInactive = 0,
        pinFunctionDigitalInput = 2,
        pinFunctionDigitalOutput = 4,
        pinFunctionAnalogInput = 8,
        pinFunctionAnalogOutput = 16,
        pinInvert = 128,
        pinFunctionTriggeredInput = 32
    }

    // Key modifiers
    public enum keyModifiers
    {
        CtrlMask = 1,
        ShiftMask = 2,
        AltMask = 4,
        WinMask = 8,
        AltGrMask = 64
    }
    #endregion Enums
    
    public partial class Form1 : Form
    {
        //internal static FormPinsConfigure formPinConfigure;
        internal static Form1 form1;

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FreeConsole();
        utils Utils = new utils();

        Dictionary<int, string> comboBoxListPokeys = new Dictionary<int, string>();
        public static Dictionary<string, Keyboard.DirectXKeyStrokes> PokeyDirectXKeybTranslation = new Dictionary<string, Keyboard.DirectXKeyStrokes>();
        public static SortedList<int, pokey> pokeysList = new SortedList<int, pokey>();
        public static SortedList<int, SortedList<int, pokeyPinConfig>> pokeyPinConfig_list = new SortedList<int, SortedList<int, pokeyPinConfig>>();
        public static SortedList<string, keyCode> importKeystroke = new SortedList<string, keyCode>();
        


        bool inGame = false;
        bool keyCommandAlreadySent = false;
        public static int currentPokeyConfigure = -1;
        public static int currentPinConfigure = -1;


        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint SendInput(uint nInputs, Input[] pInputs, int cbSize);

        [DllImport("user32.dll")]
        public static extern IntPtr GetMessageExtraInfo();

        public static string programVersion = "1.2";
        public static Thread checkVersion = null;



        private Reader falconReader;
        private Thread readThread = null;




        public Form1()
        {
            InitializeComponent();
            form1 = this;


            falconReader = new Reader(FalconDataFormats.BMS4);
            /* get all avalaible port list */

            /* falcon serial thread */
            readThread = new Thread(falconIsRunning);
            readThread.Start();

            /*activer cette ligne en cas de débug + Utils.Dbg("blablabla"); */
            //AllocConsole();

            Utils.Dbg("Go !");
            //c'est parti ! 
            startProcess();


            /* affiche la liste des pokeys dans le selecteur */
            #region afficheinSelector
            comboBoxListPokeys.Add(-1, "");
            foreach (var i in pokeysList) {
                string text = "Device #" + i.Value.id.ToString() + " UserID:" + i.Value.userId.ToString() + " name:" + i.Value.deviceName;
                comboBoxListPokeys.Add(i.Value.id, text);
            }
            selectListPokeys.DataSource = new BindingSource(comboBoxListPokeys, null);
            selectListPokeys.DisplayMember = "Value";
            selectListPokeys.ValueMember = "Key";
            #endregion afficheinSelector


            importKeystroke.Add("", new keyCode ());

        }


        public void startProcess()
        {
            // 1 - on liste les pokeys
            Utils.listPokeys();
            // 2 - on récupère les données de la sauvegarde
            if(Properties.Settings.Default.pinLinks != "") pokeyPinConfig_list = JsonConvert.DeserializeObject<SortedList<int, SortedList<int, pokeyPinConfig>>>(Properties.Settings.Default.pinLinks);
            // 3 - récupération de la liste complête des pokeys.
            Utils.GetAllPokeysConfiguration();
            // 4 - on effectue les affectation de touches
            Utils.pokeyDirectXKeyTranslate();
            // 5 - on check la version
            checkVersion = new Thread(Utils.onStartdetectVersion);
            checkVersion.Start();

            // envoyer les données des le démarrage
            //Utils.sendAllKeyCommandToF4();

        }


        private void falconIsRunning()
        {
            
            while (true)
            {
                if (falconReader.IsFalconRunning)
                {
                    //Utils.Dbg("Falcon is running !!");
                    Data.flightData = falconReader.GetCurrentData();

                    System.Collections.BitArray bit4 = new System.Collections.BitArray(new int[] { Data.flightData.hsiBits });
                    inGame = bit4[hex2binary("0x80000000")]; // se référer à la liste des lightBit HSI du FlightData.h : Flying		  = 0x80000000, // true if player is attached to an aircraft (i.e. not in UI state).  NOTE: Not a lamp bit
                    progressBarBmsRunnning.Invoke(() => { progressBarBmsRunnning.Value = 100; });                  
                    if(keyCommandAlreadySent == false && inGame == true)
                    {
                        //do here
                        Utils.sendAllKeyCommandToF4();
                        keyCommandAlreadySent = true;
                    }
                    keyCommandAlreadySent = inGame;
                }
                else
                {
                    inGame = false;
                    keyCommandAlreadySent = false;
                    progressBarBmsRunnning.Invoke(() => { progressBarBmsRunnning.Value = 0; });
                }
            }
        }



        private int hex2binary(string hexvalue)
        {
            string binaryval = "";
            binaryval = Convert.ToString(Convert.ToInt32(hexvalue, 16), 2);
            return (binaryval.Length - 1);
        }




        public static class Data
        {
            public static FlightData flightData = new FlightData();
            public static event EventHandler Update;

            public static void OnUpdate()
            {
                EventHandler handler = Update;
                if (handler != null)
                {
                    handler(null, null);
                }
            }


        }



        public void refreshTable()
        {
            /* important pour éviter des erreurs... */
            currentPinConfigure = -1;
            currentPokeyConfigure = -1;

            int key = ((KeyValuePair<int, string>)selectListPokeys.SelectedItem).Key; //récupération de l'id de la pokey sélectionnée dans la liste de choix
            Utils.Dbg("Sélection des données sur la carte pokey ID : "+key.ToString());
            if (pokeyPinConfig_list.ContainsKey(key))
            {
                tableLayoutPokey.Controls.Clear();
                tableLayoutPokey.RowStyles.Clear();


                /* HIDE TABLE */
                tableLayoutPokey.Visible = false;
                tableLayoutPokey.SuspendLayout();
                /* ADDIN NEW LINE */
                tableLayoutPokey.RowCount = tableLayoutPokey.RowCount + 1;
                tableLayoutPokey.GrowStyle = TableLayoutPanelGrowStyle.AddRows;

          
                /* Set font for labels */
                var labelFont = new Font("Open Sans", 8, FontStyle.Regular);
                /* Col 1 */
                tableLayoutPokey.Controls.Add(new Label() { Text = "Pin", Font = labelFont }, 0, tableLayoutPokey.RowCount - 1);
                /* Col 2 */
                tableLayoutPokey.Controls.Add(new Label() { Text = "Etat", Font = labelFont }, 1, tableLayoutPokey.RowCount - 1);
                /* Col 3 */
                tableLayoutPokey.Controls.Add(new Label() { Text = "Fonction", Font = labelFont }, 2, tableLayoutPokey.RowCount - 1);
                /* Col 4 */
                tableLayoutPokey.Controls.Add(new Label() { Text = "Configurer", Font = labelFont }, 3, tableLayoutPokey.RowCount - 1);


                // Processing Code


                tableLayoutPokey.MaximumSize = new Size(tableLayoutPokey.Width, tableLayoutPokey.Height);
                tableLayoutPokey.AutoScroll = true;

                tableLayoutPokey.ResumeLayout();
                tableLayoutPokey.Visible = true;




                for (int i = 0; i < pokeyPinConfig_list[key].Count; i++) // loop sur la liste de configuration de la pokey en question
                {

                    /* HIDE TABLE */
                    tableLayoutPokey.Visible = false;
                    tableLayoutPokey.SuspendLayout();
                    /* ADDIN NEW LINE */
                    tableLayoutPokey.RowCount = tableLayoutPokey.RowCount + 1;
                    tableLayoutPokey.GrowStyle = TableLayoutPanelGrowStyle.AddRows;

                    /* --- BUTTON CONFIGURER ---*/
                    Button buttonConfigure = new Button()
                    {
                        Text = "Configure",
                        Name = pokeyPinConfig_list[key][i].id.ToString(),
                        Tag = pokeyPinConfig_list[key][i].pin
                    };
                    buttonConfigure.Click += new EventHandler(Configure);


                    string fonctionOn = "";
                    string fonctionOff = "";

                    List<string> KeyOnModifier = new List<string>();
                    List<string> KeyOffModifier = new List<string>();

                    if(pokeyPinConfig_list[key][i].modifierCtrlOn) KeyOnModifier.Add("Ctrl");
                    if(pokeyPinConfig_list[key][i].modifierShitOn) KeyOnModifier.Add("Shift");
                    if(pokeyPinConfig_list[key][i].modifierAltOn) KeyOnModifier.Add("Alt");
                    if(pokeyPinConfig_list[key][i].modifierAltGrOn) KeyOnModifier.Add("AltGr");
                    if(pokeyPinConfig_list[key][i].keyCodeOn != 0) KeyOnModifier.Add(pokeyPinConfig_list[key][i].keyCodeOn.ToString().Replace("key_","").ToUpper());
                    fonctionOn = String.Join(" + ", KeyOnModifier.ToArray());

                    if(pokeyPinConfig_list[key][i].modifierCtrlOff) KeyOffModifier.Add("Ctrl");
                    if(pokeyPinConfig_list[key][i].modifierShitOff) KeyOffModifier.Add("Shift");
                    if(pokeyPinConfig_list[key][i].modifierAltOff) KeyOffModifier.Add("Alt");
                    if(pokeyPinConfig_list[key][i].modifierAltGrOff) KeyOffModifier.Add("AltGr");
                    if(pokeyPinConfig_list[key][i].keyCodeOff != 0) KeyOffModifier.Add(pokeyPinConfig_list[key][i].keyCodeOff.ToString().Replace("key_", "").ToUpper());
                    fonctionOff = String.Join(" + ", KeyOffModifier.ToArray());

                    string OnOffText = " ON [ " + fonctionOn + " ] OFF [ " + fonctionOff + " ]";
                    if (pokeyPinConfig_list[key][i].linkedTo != -1) OnOffText += " Linked with pin " + (pokeyPinConfig_list[key][i].linkedTo+1).ToString();
                    Utils.Dbg(OnOffText);

                    /* Col 1 */
                    tableLayoutPokey.Controls.Add(new Label() { Text = (pokeyPinConfig_list[key][i].pin + 1).ToString(), Font = labelFont }, 0, tableLayoutPokey.RowCount - 1);
                    /* Col 2 */
                    tableLayoutPokey.Controls.Add(new Label() { Text = pokeyPinConfig_list[key][i].etat.ToString(), Font = labelFont }, 1, tableLayoutPokey.RowCount - 1);
                    /* Col 3 */
                    tableLayoutPokey.Controls.Add(new Label() { Text = OnOffText, Font = labelFont,AutoSize = true }, 2, tableLayoutPokey.RowCount - 1);
                    /* Col 4 */
                    tableLayoutPokey.Controls.Add(buttonConfigure, 3, tableLayoutPokey.RowCount - 1);


                    // Processing Code


                    

                    tableLayoutPokey.MaximumSize = new Size(tableLayoutPokey.Width, tableLayoutPokey.Height);
                    tableLayoutPokey.AutoScroll = true;

                    tableLayoutPokey.ResumeLayout();
                    tableLayoutPokey.Visible = true;


                }
            }

        }


        

        private void Configure(object sender, EventArgs e)
        {
            Button button = sender as Button;
            int idButon = Int32.Parse(button.Tag.ToString());
            int idPokey = Int32.Parse(button.Name.ToString());
            Utils.Dbg("Test du bouton du pin : " + idButon);

            currentPinConfigure = (byte) idButon; // important pour définir le pin en cours de modification
            currentPokeyConfigure = (byte)idPokey; // important pour définir la pokey en cours de modification

            FormPinsConfigure fpc = new FormPinsConfigure();
            fpc.Text = "Configuration du pin " + (idButon + 1);
            fpc.ShowDialog(); // Shows Form2
            fpc.Left = this.Left + 50;
            fpc.Top = this.Top + 50;
            

        }




        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            /* afficher dans la tableau la configuration des pokeys*/
            refreshTable();
        }

        private void tableLayoutPokey_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonUpdateCartePokey_Click(object sender, EventArgs e)
        {
            Utils.updatePokeyPinConfiguration();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            /* sauvegarde des paramètres */
            Properties.Settings.Default["pinLinks"] = JsonConvert.SerializeObject(pokeyPinConfig_list);
            Properties.Settings.Default.Save();
            readThread.Abort(); // Stop le Thread Falcon BMS
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.buttonTest.Enabled = false;
            Utils.GetAllPokeysConfiguration();
            Utils.sendAllKeyCommandToF4();
            Thread.Sleep(5000); // 5 sec
            this.buttonTest.Enabled = true;
        }

        private void buttonLoadBMSKeystrokes_Click(object sender, EventArgs e)
        {
            loadBmsKeystrokes sal = new loadBmsKeystrokes();
            sal.loadSettings(this);
        }
    }



    public static class ControlExtensions
    {
        public static void Invoke(this Control control, Action action)
        {
            if (control.InvokeRequired) control.Invoke(new MethodInvoker(action), null);
            else action.Invoke();
        }
    }


}
