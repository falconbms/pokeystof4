﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokeysToF4
{

    public partial class FormPinsConfigure : Form
    {        
        utils Utils = new utils();
        public static Dictionary<string, string> comboBoxListKeyCode = new Dictionary<string, string>();

        private byte currentPokeyConfigure;
        private byte currentPinConfigure;

        public FormPinsConfigure()
        {
            InitializeComponent();

            //mise à jour de la liste des modifiers / keycode
            comboBoxListKeyCode.Clear();
            Utils.loadListKeyCode();
            selectListKeyCodeOn.DataSource = new BindingSource(comboBoxListKeyCode, null);
            selectListKeyCodeOn.DisplayMember = "Key";
            selectListKeyCodeOn.ValueMember = "Value";
            selectListKeyCodeOff.DataSource = new BindingSource(comboBoxListKeyCode, null);
            selectListKeyCodeOff.DisplayMember = "Key";
            selectListKeyCodeOff.ValueMember = "Value";

            currentPokeyConfigure = (byte) PokeysToF4.Form1.currentPokeyConfigure;
            currentPinConfigure = (byte) PokeysToF4.Form1.currentPinConfigure;

            for (int i = 1; i <= 55; i++) selectLinkedToPin.Items.Add(i);


            selectKeyOnKeystroke.DataSource = new BindingSource(Form1.importKeystroke, null);
            selectKeyOnKeystroke.DisplayMember = "Key";
            selectKeyOnKeystroke.ValueMember = "Value";


            selectKeyOffKeystroke.DataSource = new BindingSource(Form1.importKeystroke, null);
            selectKeyOffKeystroke.DisplayMember = "Key";
            selectKeyOffKeystroke.ValueMember = "Value";

        }

        private void FormPinsConfigure_Load(object sender, EventArgs e)
        {
            string KeyCodeOn = "";
            string KeyCodeOff = "";

            if(Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].keyCodeOn != 0) KeyCodeOn = Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].keyCodeOn.ToString().Replace("key_", "");
            if(Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].keyCodeOff != 0)  KeyCodeOff = Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].keyCodeOff.ToString().Replace("key_", "");

            if (Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].modifierCtrlOn) checkBoxModifierCtrlOn.Checked = true;
            if (Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].modifierShitOn) checkBoxModifierShiftOn.Checked = true;
            if (Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].modifierAltOn) checkBoxModifierAltOn.Checked = true;
            if (Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].modifierAltGrOn) checkBoxModifierAltGrOn.Checked = true;

            if (Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].modifierCtrlOff) checkBoxModifierCtrlOff.Checked = true;
            if (Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].modifierShitOff) checkBoxModifierShiftOff.Checked = true;
            if (Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].modifierAltOff) checkBoxModifierAltOff.Checked = true;
            if (Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].modifierAltGrOff) checkBoxModifierAltGrOff.Checked = true;


            selectListKeyCodeOn.SelectedText = KeyCodeOn;
            selectListKeyCodeOff.SelectedText = KeyCodeOff;

            if(Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].linkedTo != -1) selectLinkedToPin.SelectedText = (Form1.pokeyPinConfig_list[currentPokeyConfigure][currentPinConfigure].linkedTo + 1).ToString();

        }

        private void ButtonAppliquer_Click(object sender, EventArgs e)
        {
            int linkedTo = -1;
            if (selectLinkedToPin.Text != "" ) linkedTo = Int32.Parse(selectLinkedToPin.Text) -1;
            Utils.Dbg("linked vaut maintenant " + linkedTo);
            Utils.setPinConfig(
                currentPokeyConfigure,
                currentPinConfigure,
                selectListKeyCodeOn.Text,
                checkBoxModifierCtrlOn.Checked,
                checkBoxModifierShiftOn.Checked,
                checkBoxModifierAltOn.Checked,
                checkBoxModifierAltGrOn.Checked,
                selectListKeyCodeOff.Text,
                checkBoxModifierCtrlOff.Checked,
                checkBoxModifierShiftOff.Checked,
                checkBoxModifierAltOff.Checked,
                checkBoxModifierAltGrOff.Checked,
                linkedTo
                );
            Utils.updatePokeyPinConfiguration();
            Form1.form1.refreshTable();
            this.Close();
        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void Switch_Enter(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void selectListKeyCodeOn_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void selectListKeyCodeOff_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void linkKeyCodeOn_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click_1(object sender, EventArgs e)
        {

        }

        private void selectKeyOnKeystroke_SelectedIndexChanged(object sender, EventArgs e)
        {
            //selectKeyOnKeystroke.SelectedValue
            if (selectKeyOnKeystroke.Text != "")
            {
                var tmp = (keyCode)Form1.importKeystroke.Values.ElementAt(selectKeyOnKeystroke.SelectedIndex);
                Utils.Dbg("Selectionne " + tmp.key);
                selectListKeyCodeOn.Text = tmp.key;
                checkBoxModifierCtrlOn.Checked = tmp.modifierCtrl;
                checkBoxModifierShiftOn.Checked = tmp.modifierShift;
                checkBoxModifierAltOn.Checked = tmp.modifierAlt;
                checkBoxModifierAltGrOn.Checked = tmp.modifierAltGr;
            }
            else
            {
                selectListKeyCodeOn.Text = "";
                checkBoxModifierCtrlOn.Checked = false;
                checkBoxModifierShiftOn.Checked = false;
                checkBoxModifierAltOn.Checked = false;
                checkBoxModifierAltGrOn.Checked = false;
            }

        }



        private void selectKeyOffKeystroke_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //selectKeyOnKeystroke.SelectedValue
            if (selectKeyOffKeystroke.Text != "")
            {
                var tmp = (keyCode)Form1.importKeystroke.Values.ElementAt(selectKeyOffKeystroke.SelectedIndex);
                Utils.Dbg("Selectionne " + tmp.key);
                selectListKeyCodeOff.Text = tmp.key;
                checkBoxModifierCtrlOff.Checked = tmp.modifierCtrl;
                checkBoxModifierShiftOff.Checked = tmp.modifierShift;
                checkBoxModifierAltOff.Checked = tmp.modifierAlt;
                checkBoxModifierAltGrOff.Checked = tmp.modifierAltGr;
            }
            else
            {
                selectListKeyCodeOff.Text = "";
                checkBoxModifierCtrlOff.Checked = false;
                checkBoxModifierShiftOff.Checked = false;
                checkBoxModifierAltOff.Checked = false;
                checkBoxModifierAltGrOff.Checked = false;
            }
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }
    }
}
