﻿namespace PokeysToF4
{
    partial class FormPinsConfigure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonAppliquer = new System.Windows.Forms.Button();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.selectListKeyCodeOn = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.selectListKeyCodeOff = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxModifierCtrlOn = new System.Windows.Forms.CheckBox();
            this.checkBoxModifierShiftOn = new System.Windows.Forms.CheckBox();
            this.checkBoxModifierAltOn = new System.Windows.Forms.CheckBox();
            this.checkBoxModifierAltGrOn = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxModifierAltGrOff = new System.Windows.Forms.CheckBox();
            this.checkBoxModifierAltOff = new System.Windows.Forms.CheckBox();
            this.checkBoxModifierShiftOff = new System.Windows.Forms.CheckBox();
            this.checkBoxModifierCtrlOff = new System.Windows.Forms.CheckBox();
            this.Switch = new System.Windows.Forms.GroupBox();
            this.selectLinkedToPin = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.selectKeyOffKeystroke = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.selectKeyOnKeystroke = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Switch.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonAppliquer
            // 
            this.ButtonAppliquer.Location = new System.Drawing.Point(12, 566);
            this.ButtonAppliquer.Name = "ButtonAppliquer";
            this.ButtonAppliquer.Size = new System.Drawing.Size(75, 23);
            this.ButtonAppliquer.TabIndex = 0;
            this.ButtonAppliquer.Text = "Appliquer";
            this.ButtonAppliquer.UseVisualStyleBackColor = true;
            this.ButtonAppliquer.Click += new System.EventHandler(this.ButtonAppliquer_Click);
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Location = new System.Drawing.Point(423, 566);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(75, 23);
            this.buttonAnnuler.TabIndex = 1;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnuler_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "KeyCode";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.selectListKeyCodeOn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(234, 96);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Key Code (On)";
            // 
            // selectListKeyCodeOn
            // 
            this.selectListKeyCodeOn.FormattingEnabled = true;
            this.selectListKeyCodeOn.Location = new System.Drawing.Point(99, 37);
            this.selectListKeyCodeOn.Name = "selectListKeyCodeOn";
            this.selectListKeyCodeOn.Size = new System.Drawing.Size(121, 21);
            this.selectListKeyCodeOn.TabIndex = 4;
            this.selectListKeyCodeOn.SelectedIndexChanged += new System.EventHandler(this.selectListKeyCodeOn_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.selectListKeyCodeOff);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Location = new System.Drawing.Point(263, 27);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(234, 96);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Key Code (Off)";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // selectListKeyCodeOff
            // 
            this.selectListKeyCodeOff.FormattingEnabled = true;
            this.selectListKeyCodeOff.Location = new System.Drawing.Point(99, 37);
            this.selectListKeyCodeOff.Name = "selectListKeyCodeOff";
            this.selectListKeyCodeOff.Size = new System.Drawing.Size(121, 21);
            this.selectListKeyCodeOff.TabIndex = 4;
            this.selectListKeyCodeOff.SelectedIndexChanged += new System.EventHandler(this.selectListKeyCodeOff_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "KeyCode";
            // 
            // checkBoxModifierCtrlOn
            // 
            this.checkBoxModifierCtrlOn.AutoSize = true;
            this.checkBoxModifierCtrlOn.Location = new System.Drawing.Point(21, 33);
            this.checkBoxModifierCtrlOn.Name = "checkBoxModifierCtrlOn";
            this.checkBoxModifierCtrlOn.Size = new System.Drawing.Size(54, 17);
            this.checkBoxModifierCtrlOn.TabIndex = 0;
            this.checkBoxModifierCtrlOn.Text = "CTRL";
            this.checkBoxModifierCtrlOn.UseVisualStyleBackColor = true;
            // 
            // checkBoxModifierShiftOn
            // 
            this.checkBoxModifierShiftOn.AutoSize = true;
            this.checkBoxModifierShiftOn.Location = new System.Drawing.Point(21, 66);
            this.checkBoxModifierShiftOn.Name = "checkBoxModifierShiftOn";
            this.checkBoxModifierShiftOn.Size = new System.Drawing.Size(57, 17);
            this.checkBoxModifierShiftOn.TabIndex = 1;
            this.checkBoxModifierShiftOn.Text = "SHIFT";
            this.checkBoxModifierShiftOn.UseVisualStyleBackColor = true;
            // 
            // checkBoxModifierAltOn
            // 
            this.checkBoxModifierAltOn.AutoSize = true;
            this.checkBoxModifierAltOn.Location = new System.Drawing.Point(21, 99);
            this.checkBoxModifierAltOn.Name = "checkBoxModifierAltOn";
            this.checkBoxModifierAltOn.Size = new System.Drawing.Size(46, 17);
            this.checkBoxModifierAltOn.TabIndex = 2;
            this.checkBoxModifierAltOn.Text = "ALT";
            this.checkBoxModifierAltOn.UseVisualStyleBackColor = true;
            // 
            // checkBoxModifierAltGrOn
            // 
            this.checkBoxModifierAltGrOn.AutoSize = true;
            this.checkBoxModifierAltGrOn.Location = new System.Drawing.Point(21, 132);
            this.checkBoxModifierAltGrOn.Name = "checkBoxModifierAltGrOn";
            this.checkBoxModifierAltGrOn.Size = new System.Drawing.Size(65, 17);
            this.checkBoxModifierAltGrOn.TabIndex = 3;
            this.checkBoxModifierAltGrOn.Text = "ALT GR";
            this.checkBoxModifierAltGrOn.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxModifierAltGrOn);
            this.groupBox2.Controls.Add(this.checkBoxModifierAltOn);
            this.groupBox2.Controls.Add(this.checkBoxModifierShiftOn);
            this.groupBox2.Controls.Add(this.checkBoxModifierCtrlOn);
            this.groupBox2.Location = new System.Drawing.Point(12, 381);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(234, 169);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Modifiers (On)";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxModifierAltGrOff);
            this.groupBox3.Controls.Add(this.checkBoxModifierAltOff);
            this.groupBox3.Controls.Add(this.checkBoxModifierShiftOff);
            this.groupBox3.Controls.Add(this.checkBoxModifierCtrlOff);
            this.groupBox3.Location = new System.Drawing.Point(264, 381);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(233, 169);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Modifiers (Off)";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // checkBoxModifierAltGrOff
            // 
            this.checkBoxModifierAltGrOff.AutoSize = true;
            this.checkBoxModifierAltGrOff.Location = new System.Drawing.Point(21, 132);
            this.checkBoxModifierAltGrOff.Name = "checkBoxModifierAltGrOff";
            this.checkBoxModifierAltGrOff.Size = new System.Drawing.Size(65, 17);
            this.checkBoxModifierAltGrOff.TabIndex = 3;
            this.checkBoxModifierAltGrOff.Text = "ALT GR";
            this.checkBoxModifierAltGrOff.UseVisualStyleBackColor = true;
            // 
            // checkBoxModifierAltOff
            // 
            this.checkBoxModifierAltOff.AutoSize = true;
            this.checkBoxModifierAltOff.Location = new System.Drawing.Point(21, 99);
            this.checkBoxModifierAltOff.Name = "checkBoxModifierAltOff";
            this.checkBoxModifierAltOff.Size = new System.Drawing.Size(46, 17);
            this.checkBoxModifierAltOff.TabIndex = 2;
            this.checkBoxModifierAltOff.Text = "ALT";
            this.checkBoxModifierAltOff.UseVisualStyleBackColor = true;
            // 
            // checkBoxModifierShiftOff
            // 
            this.checkBoxModifierShiftOff.AutoSize = true;
            this.checkBoxModifierShiftOff.Location = new System.Drawing.Point(21, 66);
            this.checkBoxModifierShiftOff.Name = "checkBoxModifierShiftOff";
            this.checkBoxModifierShiftOff.Size = new System.Drawing.Size(57, 17);
            this.checkBoxModifierShiftOff.TabIndex = 1;
            this.checkBoxModifierShiftOff.Text = "SHIFT";
            this.checkBoxModifierShiftOff.UseVisualStyleBackColor = true;
            // 
            // checkBoxModifierCtrlOff
            // 
            this.checkBoxModifierCtrlOff.AutoSize = true;
            this.checkBoxModifierCtrlOff.Location = new System.Drawing.Point(21, 33);
            this.checkBoxModifierCtrlOff.Name = "checkBoxModifierCtrlOff";
            this.checkBoxModifierCtrlOff.Size = new System.Drawing.Size(54, 17);
            this.checkBoxModifierCtrlOff.TabIndex = 0;
            this.checkBoxModifierCtrlOff.Text = "CTRL";
            this.checkBoxModifierCtrlOff.UseVisualStyleBackColor = true;
            // 
            // Switch
            // 
            this.Switch.Controls.Add(this.selectLinkedToPin);
            this.Switch.Controls.Add(this.label3);
            this.Switch.Location = new System.Drawing.Point(12, 261);
            this.Switch.Name = "Switch";
            this.Switch.Size = new System.Drawing.Size(485, 100);
            this.Switch.TabIndex = 7;
            this.Switch.TabStop = false;
            this.Switch.Text = "ON-OFF-ON Switch";
            this.Switch.Enter += new System.EventHandler(this.Switch_Enter);
            // 
            // selectLinkedToPin
            // 
            this.selectLinkedToPin.FormattingEnabled = true;
            this.selectLinkedToPin.Location = new System.Drawing.Point(144, 46);
            this.selectLinkedToPin.Name = "selectLinkedToPin";
            this.selectLinkedToPin.Size = new System.Drawing.Size(43, 21);
            this.selectLinkedToPin.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Pin linked to another pin";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.selectKeyOffKeystroke);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.selectKeyOnKeystroke);
            this.groupBox5.Location = new System.Drawing.Point(13, 134);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(485, 121);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Get from Keystroke";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "KeyCode Off";
            // 
            // selectKeyOffKeystroke
            // 
            this.selectKeyOffKeystroke.FormattingEnabled = true;
            this.selectKeyOffKeystroke.Location = new System.Drawing.Point(84, 75);
            this.selectKeyOffKeystroke.Name = "selectKeyOffKeystroke";
            this.selectKeyOffKeystroke.Size = new System.Drawing.Size(386, 21);
            this.selectKeyOffKeystroke.TabIndex = 14;
            this.selectKeyOffKeystroke.SelectedIndexChanged += new System.EventHandler(this.selectKeyOffKeystroke_SelectedIndexChanged_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "KeyCode On";
            // 
            // selectKeyOnKeystroke
            // 
            this.selectKeyOnKeystroke.FormattingEnabled = true;
            this.selectKeyOnKeystroke.Location = new System.Drawing.Point(84, 36);
            this.selectKeyOnKeystroke.Name = "selectKeyOnKeystroke";
            this.selectKeyOnKeystroke.Size = new System.Drawing.Size(386, 21);
            this.selectKeyOnKeystroke.TabIndex = 12;
            this.selectKeyOnKeystroke.SelectedIndexChanged += new System.EventHandler(this.selectKeyOnKeystroke_SelectedIndexChanged);
            // 
            // FormPinsConfigure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 599);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.Switch);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonAnnuler);
            this.Controls.Add(this.ButtonAppliquer);
            this.Name = "FormPinsConfigure";
            this.Text = "Configurer un pin";
            this.Load += new System.EventHandler(this.FormPinsConfigure_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Switch.ResumeLayout(false);
            this.Switch.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonAppliquer;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox selectListKeyCodeOn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox selectListKeyCodeOff;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxModifierCtrlOn;
        private System.Windows.Forms.CheckBox checkBoxModifierShiftOn;
        private System.Windows.Forms.CheckBox checkBoxModifierAltOn;
        private System.Windows.Forms.CheckBox checkBoxModifierAltGrOn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxModifierAltGrOff;
        private System.Windows.Forms.CheckBox checkBoxModifierAltOff;
        private System.Windows.Forms.CheckBox checkBoxModifierShiftOff;
        private System.Windows.Forms.CheckBox checkBoxModifierCtrlOff;
        private System.Windows.Forms.GroupBox Switch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox selectLinkedToPin;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox selectKeyOffKeystroke;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox selectKeyOnKeystroke;
    }
}