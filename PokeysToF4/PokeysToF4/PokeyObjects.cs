﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokeysToF4
{

    
    public class pokey
    {
        public int _id; 
        public byte _userID;
        public int _sn;
        public int _versionMajor;
        public int _versionMinor;
        public string _deviceName;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public byte userId
        {
            get { return _userID; }
            set { _userID = value; }
        }

        public string deviceName
        {
            get { return _deviceName; }
            set { _deviceName = value; }
        }

        public int sn
        {
            get { return _sn; }
            set { _sn = value; }
        }

        public int versionMajor
        {
            get { return _versionMajor; }
            set { _versionMajor = value; }
        }

        public int versionMinor
        {
            get { return _versionMinor; }
            set { _versionMinor = value; }
        }
    }

    public class keyCode
    {
        public string _keyCode;
        public bool _modifierCtrl = false;
        public bool _modifierShift = false;
        public bool _modifierAlt = false;
        public bool _modifierAltGr = false;

        public string key
        {
            get { return _keyCode; }
            set { _keyCode = value; }
        }

        public bool modifierShift
        {
            get { return _modifierShift; }
            set { _modifierShift = value; }
        }


        public bool modifierCtrl
        {
            get { return _modifierCtrl; }
            set { _modifierCtrl = value; }
        }

        public bool modifierAlt
        {
            get { return _modifierAlt; }
            set { _modifierAlt = value; }
        }

        public bool modifierAltGr
        {
            get { return _modifierAltGr; }
            set { _modifierAltGr = value; }
        }
    }








    public class pokeyPinConfig
    {
        public int _id;
        public int _pin;
        public bool _etat;
        public int _linkedTo = -1;

        public ekeyCodes _keyCodeOn;
        public bool _modifierCtrlOn = false;
        public bool _modifierShiftOn = false;
        public bool _modifierAltOn = false;
        public bool _modifierAltGrOn = false;

        public ekeyCodes _keyCodeOff;
        public bool _modifierCtrlOff = false;
        public bool _modifierShiftOff = false;
        public bool _modifierAltOff = false;
        public bool _modifierAltGrOff = false;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int pin
        {
            get { return _pin; }
            set { _pin = value; }
        }

        public bool etat
        {
            get { return _etat; }
            set { _etat = value; }
        }


        public int linkedTo
        {
            get { return _linkedTo; }
            set { _linkedTo = value; }
        }

        /*
         * 
         * 
         * 
        public bool link
        {
            get { return _link; }
            set { _link = value; }
        }

        public bool linkKeyCode
        {
            get { return _linkKeyCode; }
            set { _linkKeyCode = value; }
        }

        public int linkId
        {
            get { return _linkId; }
            set { _linkId = value; }
        }
        */
        public ekeyCodes keyCodeOn
        {
            get { return _keyCodeOn; }
            set { _keyCodeOn = value; }
        }

        public bool modifierCtrlOn
        {
            get { return _modifierCtrlOn; }
            set { _modifierCtrlOn = value; }
        }

        public bool modifierAltOn
        {
            get { return _modifierAltOn; }
            set { _modifierAltOn = value; }
        }

        public bool modifierAltGrOn
        {
            get { return _modifierAltGrOn; }
            set { _modifierAltGrOn = value; }
        }

        public bool modifierShitOn
        {
            get { return _modifierShiftOn; }
            set { _modifierShiftOn = value; }
        }


        public ekeyCodes keyCodeOff
        {
            get { return _keyCodeOff; }
            set { _keyCodeOff = value; }
        }

        public bool modifierCtrlOff
        {
            get { return _modifierCtrlOff; }
            set { _modifierCtrlOff = value; }
        }

        public bool modifierAltOff
        {
            get { return _modifierAltOff; }
            set { _modifierAltOff = value; }
        }

        public bool modifierAltGrOff
        {
            get { return _modifierAltGrOff; }
            set { _modifierAltGrOff = value; }
        }

        public bool modifierShitOff
        {
            get { return _modifierShiftOff; }
            set { _modifierShiftOff = value; }
        }

    }


    
}
